const express = require('express');
const bodyParser = require('body-parser');
const contactoRoutes = require('./routes/contactosRoute.js');
const tasksRoute = require('./routes/taskRoute');
const authRoute = require('./routes/authRoute');
const tasktypeRoute = require('./routes/taskTypeRoute.js');
const sequelize = require('./db.js');
const cors = require('cors'); 
const helmet = require('helmet'); // Agrega el paquete helmet para encabezados de seguridad

const app = express();
const PORT = process.env.PORT || 8800;

const corsOptions = {
  origin: process.env.URL_FRONTEND, // Dominio permitido
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE', // Métodos permitidos
};

app.use(cors(corsOptions));
app.use(helmet()); // Agrega encabezados de seguridad
app.use(bodyParser.json());
app.use('/contacto', contactoRoutes);
app.use('/task', tasksRoute);
app.use('/auth', authRoute);
app.use('/tasktype', tasktypeRoute);

sequelize
  .sync()
  .then(() => {
    console.log('Base de datos conectada y sincronizada.');
    app.listen(PORT, () => {
      console.log(`Servidor Express escuchando en el puerto ${PORT}`);
    });
  })
  .catch((error) => {
    console.error('Error al sincronizar la base de datos:', error);
  });
